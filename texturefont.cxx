/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "texturefont.hxx"

#include <ft2build.h>
#include FT_FREETYPE_H

namespace GLpp
{
    GLuint TextureFont::VAO;
    GLuint TextureFont::vertices;
    GLuint TextureFont::vertexShader;
    GLuint TextureFont::fragmentShader;
    GLuint TextureFont::program;
    TextureFont::Uniforms TextureFont::uniforms;
    TextureFont::Attribs TextureFont::attribs;

    GLvoid TextureFont::DrawGlyph(GLubyte* bitmap, GLubyte* image, const GLuint& bitWidth, const GLuint& width, const GLuint& height)
    {
        for (GLuint j = 0; j < height; j++)
        {
            for (GLuint k = 0; k < width; k++)
            {
                if (k > bitWidth)
                {
                    image[(j * width) + k] = 0;
                } else
                {
                    image[(j * width) + k] = bitmap[(j * width) + k];
                }
            }
        }
    }

    GLboolean TextureFont::Initiate()
    {
        // Interleaved: vec2 vertex, vec2 texCoord
        GLfloat point[] = {-0.5f, 0.5f,
                            0.0f, 0.0f,
                           -0.5f, -0.5f,
                            0.0f, 1.0f,
                            0.5f, -0.5f,
                            1.0f, 1.0f,
                            0.5f, 0.5f,
                            1.0f, 0.0f};

        GLint compiled;

        vertexShader = glCreateShader(GL_VERTEX_SHADER);
        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        program = glCreateProgram();

#include "texturefont_vert.h"

#include "texturefont_frag.h"

        GLchar* vertSource = new GLchar[texturefont_vert_len];
        for (GLuint i = 0; i < texturefont_vert_len; i++)
        {
            vertSource[i] = texturefont_vert[i];
        }

        GLchar* fragSource = new GLchar[texturefont_frag_len];
        for (GLuint i = 0; i < texturefont_frag_len; i++)
        {
            fragSource[i] = texturefont_frag[i];
        }
        const GLint vertlength = texturefont_vert_len;
        const GLint fraglength = texturefont_frag_len;

        const GLchar* ptr = const_cast<const GLchar*>(vertSource);

        glShaderSource(vertexShader, 1, &ptr, &vertlength);
        delete[] (vertSource);
        glCompileShader(vertexShader);
        glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &compiled);
        if (!compiled)
        {
            GLint length;
            GLchar* log;
            glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &length);
            log = new GLchar[length];
            glGetShaderInfoLog(vertexShader, length, &length, log);
            std::cerr << "Vertex shader compilation failed:" << std::endl << log << std::endl;
            delete[] (log);
            return (GL_FALSE);
        }

        ptr = const_cast<const GLchar*>(fragSource);
        glShaderSource(fragmentShader, 1, &ptr, &fraglength);
        delete[] (fragSource);
        glCompileShader(fragmentShader);
        glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &compiled);
        if (!compiled)
        {
            GLint length;
            GLchar* log;
            glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &length);
            log = new GLchar[length];
            glGetShaderInfoLog(fragmentShader, length, &length, log);
            std::cerr << "Fragment shader compilation failed:" << std::endl << log << std::endl;
            delete[] (log);
            return (GL_FALSE);
        }

        GLint linked;

        glAttachShader(program, vertexShader);
        glAttachShader(program, fragmentShader);

        glLinkProgram(program);

        glGetProgramiv(program, GL_LINK_STATUS, &linked);

        if (linked)
        {
            glUseProgram(program);
        } else
        {
            GLint length;
            GLchar* log;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
            log = new GLchar[length];
            glGetProgramInfoLog(program, length, &length, log);
            std::cerr << "Program linking failed:" << std::endl << log << std::endl;
            delete[] (log);
            return (GL_FALSE);
        }

        uniforms.color = glGetUniformLocation(program, "color");
        uniforms.windowDimensions = glGetUniformLocation(program, "windowDimensions");
        uniforms.size = glGetUniformLocation(program, "size");
        attribs.character = glGetAttribLocation(program, "character");
        attribs.windowLocation = glGetAttribLocation(program, "windowLocation");


        glGenVertexArrays(1, &VAO);
        glBindVertexArray(VAO);

        glGenBuffers(1, &vertices);
        glBindBuffer(GL_ARRAY_BUFFER, vertices);
        glBufferData(GL_ARRAY_BUFFER, sizeof(point), point, GL_STATIC_DRAW);

        GLuint index = glGetAttribLocation(program, "vertex");
        glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 4, nullptr);
        glEnableVertexAttribArray(index);

        index = glGetAttribLocation(program, "texCoord");
        glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 4, reinterpret_cast<const GLvoid*>(sizeof(GLfloat) * 2));
        glEnableVertexAttribArray(index);

        index = attribs.character;
        glVertexAttribIPointer(index, 1, GL_INT, sizeof(GLint) + (sizeof(GLfloat) * 2), nullptr);
        glVertexAttribDivisor(index, 1);
        glEnableVertexAttribArray(index);

        index = attribs.windowLocation;
        glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, sizeof(GLint) + (sizeof(GLfloat) * 2), reinterpret_cast<const GLvoid*>(sizeof(GLint)));
        glVertexAttribDivisor(index, 1);
        glEnableVertexAttribArray(index);

        glBindVertexArray(0);

        return (GL_TRUE);
    }

    GLvoid TextureFont::Cleanup()
    {
        glUseProgram(0);
        glDeleteProgram(program);
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDeleteVertexArrays(1, &VAO);
        glDeleteBuffers(1, &vertices);
    }

    TextureFont::TextureFont()
    {
    }

    TextureFont::~TextureFont()
    {
        glDeleteTextures(1, &texture);
        glDeleteBuffers(1, &instanceBuffer);
        delete[] (instanceData);
    }

    GLboolean TextureFont::Create(const std::string& fontName, GLuint maxSize)
    {
        instanceData = nullptr;
        instanceDataSize = 0;
        GLboolean texInited = GL_FALSE;

        FT_Library library;
        FT_Face face;

        FT_GlyphSlot slot;
        FT_Vector pen;
        FT_Error error;

        struct
        {
            GLubyte* bitmap;
            GLubyte* resizedBitmap;
            GLuint width;
            GLuint height;
            GLfloat xAdvance;
            GLfloat yAdvance;
            GLfloat yBearing;
            GLfloat xBearing;
        } glyphs[127];

        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D_ARRAY, texture);

        GLuint n;

        error = FT_Init_FreeType(&library);
        if (error)
        {
            std::cerr << "Error:  Could not initiate FreeType 2" << std::endl;
            return (GL_FALSE);
        }

        error = FT_New_Face(library, fontName.c_str(), 0, &face);
        if (error)
        {
            std::cerr << "Error:  Could not create font face from file" << std::endl;
            FT_Done_FreeType(library);
            return (GL_FALSE);
        }

        // If maxSize is not a power of two, take it up to the next greatest power of two
        for (unsigned int i = 1u; i != maxSize; i <<= 1)
        {
            if (maxSize < i)
            {
                maxSize = i;
                break;
            }
        }

        // s represents the resolution of the texture
        // n represents the mipmap level of the texture
        n = 0;

        for (unsigned int s  = maxSize; s > 0; s /= 2)
        {

            error = FT_Set_Pixel_Sizes(face, s, s);
            if (error)
            {
                std::cerr << "Error:  Could not set pixel size" << std::endl;
                FT_Done_FreeType(library);
                return (GL_FALSE);
            }
            pen.x = 0;
            pen.y = 0;
            slot = face->glyph;

            for (GLuint i = 0; i < 127; i++)
            {
                FT_Set_Transform(face, NULL, &pen);

                error = FT_Load_Char(face, i, FT_LOAD_RENDER);
                if (error)
                {
                    continue;
                }

                if (i != 32)
                {
                    glyphs[i].width = slot->bitmap.width;
                    glyphs[i].height = slot->bitmap.rows;
                    glyphs[i].bitmap = new GLubyte[glyphs[i].width * glyphs[i].height];
                    glyphs[i].xAdvance = (GLfloat)slot->metrics.horiAdvance / 64.0f;
                    glyphs[i].yAdvance = (GLfloat)slot->metrics.vertAdvance / 64.0f;
                    glyphs[i].yBearing = (GLfloat)slot->metrics.horiBearingY / 64.0f;
                    glyphs[i].xBearing = (GLfloat)slot->metrics.horiBearingX / 64.0f;
                    DrawGlyph(slot->bitmap.buffer, glyphs[i].bitmap, slot->bitmap.width, glyphs[i].width, glyphs[i].height);
                } else
                {
                    glyphs[i].width = 0;
                    glyphs[i].height = 0;
                    glyphs[i].bitmap = new GLubyte[0];
                    glyphs[i].xAdvance = (GLfloat)slot->metrics.horiAdvance / 64.0f;
                    glyphs[i].yAdvance = (GLfloat)slot->metrics.vertAdvance / 64.0f;
                    glyphs[i].yBearing = (GLfloat)slot->metrics.horiBearingY / 64.0f;
                    glyphs[i].xBearing = (GLfloat)slot->metrics.horiBearingX / 64.0f;
                }
            }
            glTexImage3D(GL_TEXTURE_2D_ARRAY,
                         n,
                         GL_ALPHA,
                         s,
                         s,
                         128,
                         0,
                         GL_ALPHA,
                         GL_UNSIGNED_BYTE,
                         nullptr);
            for (GLuint i = 0; i < 127; i++)
            {
                glyphs[i].resizedBitmap = new GLubyte[s * s];
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP);
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP);

                for (GLuint y = 0; y < s; y++)
                {
                    for (GLuint x = 0; x < s; x++)
                    {
                        if ((y < glyphs[i].height) && (x < glyphs[i].width))
                        {
                            glyphs[i].resizedBitmap[(y * s) + x] = glyphs[i].bitmap[(y * glyphs[i].width) + x];
                        } else
                        {
                            glyphs[i].resizedBitmap[(y * s) + x] = 0;
                        }
                    }
                }

                glTexSubImage3D(GL_TEXTURE_2D_ARRAY,
                             n,
                             0,
                             0,
                             i,
                             s,
                             s,
                             1,
                             GL_ALPHA,
                             GL_UNSIGNED_BYTE,
                             glyphs[i].resizedBitmap);
                if (!texInited)
                {
                    texGlyphs[i].width = (GLfloat)glyphs[i].width / (GLfloat)s;
                    texGlyphs[i].height = (GLfloat)glyphs[i].height / (GLfloat)s;
                    texGlyphs[i].xAdvance = (GLfloat)glyphs[i].xAdvance / (GLfloat)s;
                    texGlyphs[i].yAdvance = (GLfloat)glyphs[i].yAdvance / (GLfloat)s;
                    texGlyphs[i].xBearing = (GLfloat)glyphs[i].xBearing / (GLfloat)s;
                    texGlyphs[i].yBearing = (GLfloat)glyphs[i].yBearing / (GLfloat)s;
                }
                delete[] glyphs[i].bitmap;
                delete[] glyphs[i].resizedBitmap;
            }
            texInited = GL_TRUE;
            n++;
        }
        FT_Done_Face(face);
        FT_Done_FreeType(library);
        glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

        glGenBuffers(1, &instanceBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, instanceBuffer);

        return (GL_TRUE);
    }


    GLvoid TextureFont::DrawString(const std::string& text, const GLfloat& x, const GLfloat& y, const GLfloat& size, const GLboolean& reverseX, const GLboolean& reverseY, const GLfloat* color)
    {
        GLfloat xLocation, yLocation;
        GLint viewport[4];

        if (text.size() == 0)
        {
            return;
        }

        glPushAttrib(GL_COLOR_BUFFER_BIT | GL_TEXTURE_BIT | GL_DEPTH_BUFFER_BIT);
        glUseProgram(program);
        glBindVertexArray(VAO);
        glBindTexture(GL_TEXTURE_2D_ARRAY, texture);

        glEnable(GL_BLEND);
        glDisable(GL_DEPTH_TEST);
        glBlendEquation(GL_FUNC_ADD);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glGetIntegerv(GL_VIEWPORT, viewport);
        glUniform2iv(uniforms.windowDimensions, 1, &(viewport[2]));
        glUniform4fv(uniforms.color, 1, color);

        glUniform1f(uniforms.size, size);

        if (instanceDataSize < text.size())
        {
            delete[] (instanceData);
            instanceData = new InstanceData[text.size()];
        }


        if (reverseY)
        {
            yLocation = (viewport[3] - y);
        } else
        {
            yLocation = y;
        }

        if (reverseX)
        {
            xLocation = (viewport[2] - x - size);
            for (GLint i = (text.length() - 1); i >= 0; i--)
            {
                unsigned char character = text[i];

                if (character == '\n')
                {
                    yLocation += (texGlyphs[character].yAdvance * size);
                    xLocation = (viewport[2] - x - size);
                    continue;
                }

                instanceData[i].character = character;
                instanceData[i].windowLocationX = xLocation + (texGlyphs[character].xBearing * size);
                instanceData[i].windowLocationY = yLocation + (size * (texGlyphs[character].yBearing - 1.0f));

                if (i > 0)
                {
                    // Shadows other character
                    unsigned char character = text[i - 1];

                    xLocation -= (texGlyphs[character].xAdvance * size);
                }
            }
        } else
        {
            xLocation = x;
            for (GLuint i = 0; i < text.length(); i++)
            {
                unsigned char character = text[i];

                if (character == '\n')
                {
                    yLocation -= (texGlyphs[character].yAdvance * size);
                    xLocation = x;
                    continue;
                }

                instanceData[i].character = character;
                instanceData[i].windowLocationX = xLocation + (texGlyphs[character].xBearing * size);
                instanceData[i].windowLocationY = yLocation + (size * (texGlyphs[character].yBearing - 1.0f));

                xLocation += (texGlyphs[character].xAdvance * size);
            }
        }

        if (instanceDataSize < text.size())
        {
            glBindBuffer(GL_ARRAY_BUFFER, instanceBuffer);
            glBufferData(GL_ARRAY_BUFFER, sizeof(InstanceData) * text.size(), instanceData, GL_DYNAMIC_DRAW);
            instanceDataSize = text.size();

        } else
        {
            glBindBuffer(GL_ARRAY_BUFFER, instanceBuffer);
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(InstanceData) * text.size(), instanceData);
        }

        glVertexAttribIPointer(attribs.character, 1, GL_INT, sizeof(GLint) + (sizeof(GLfloat) * 2), nullptr);
        glVertexAttribDivisor(attribs.character, 1);

        glVertexAttribPointer(attribs.windowLocation, 2, GL_FLOAT, GL_FALSE, sizeof(GLint) + (sizeof(GLfloat) * 2), reinterpret_cast<const GLvoid*>(sizeof(GLint)));
        glVertexAttribDivisor(attribs.windowLocation, 1);

        glDrawArraysInstanced(GL_QUADS, 0, 4, text.size());
        glBindVertexArray(0);
        glUseProgram(0);
        glPopAttrib();
    }
}


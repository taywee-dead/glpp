Objects = texturefont.o file.o
ObjectsD = texturefont-d.o file-d.o

allObjects = $(Objects) $(ObjectsD)

Libraries = libGLpp.a libGLpp-d.a

CC = gcc
CXX = g++

CommonOpts = -Wall -Wextra -std=c++11 -fPIC

CompileOptsAll = -c $(CommonOpts) -I.
CompileOpts = $(CompileOptsAll) -s -O2
CompileOptsD = $(CompileOptsAll) -g -O0

LinkerOptsAll = $(CommonOpts) -lGL -lGLEW `freetype-config --libs` `pkg-config --libs libxml++-2.6`
LinkerOpts = $(LinkerOptsAll) -O2 -s
LinkerOptsD = $(LinkerOptsAll) -O0 -g

.PHONY : all clean debug install release shared static uninstall docs

all : $(Libraries) linkeropts.txt linkeroptsd.txt

install :
	-mv -v -t /usr/lib $(Libraries)
	cp -v --parents -t /usr/include `find . -iname "*.hxx"` 

uninstall :
	-rm -r /usr/include/GLpp
	-rm /usr/include/GLpp.hxx /usr/lib/libGLpp*.so

clean :
	-rm -v $(allObjects) $(Libraries) 

docs : 
	doxygen

libGLpp.a : $(Objects)
	ar rcs ./libGLpp.a $(Objects)
	
libGLpp-d.a : $(ObjectsD)
	ar rcs ./libGLpp-d.a $(ObjectsD)

linkeropts.txt:
	echo "$(LinkerOpts)" > linkeropts.txt

linkeroptsd.txt:
	echo "$(LinkerOptsD)" > linkeroptsd.txt

texturefont.o : texturefont.hxx texturefont.cxx texturefont_vert.h texturefont_frag.h
	$(CXX) $(CompileOpts) -o ./texturefont.o ./texturefont.cxx `freetype-config --cflags`

file.o : file.hxx file.cxx
	$(CXX) $(CompileOpts) -o ./file.o ./file.cxx `pkg-config libxml++-2.6 --cflags`

texturefont-d.o : texturefont.hxx texturefont.cxx texturefont_vert.h texturefont_frag.h
	$(CXX) $(CompileOptsD) -o ./texturefont-d.o ./texturefont.cxx `freetype-config --cflags`

file-d.o : file.hxx file.cxx
	$(CXX) $(CompileOptsD) -o ./file-d.o ./file.cxx `pkg-config libxml++-2.6 --cflags`

texturefont_vert.h : texturefont.vert
	xxd -i texturefont.vert > texturefont_vert.h

texturefont_frag.h : texturefont.frag
	xxd -i texturefont.frag > texturefont_frag.h

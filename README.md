NAME
        OpenGL++

VERSION
        2.0.0

DESCRIPTION
        A utility library, written to extend the use of OpenGL through an
        object-oriented interface.

DEPENDENCIES
        GLpp currently depends upon OpenGL, GLEW, GLmm, and freetype2.
        These libraries need to be included by the project that uses them.
        Building this project also creates 2 linkeropts*.txt files, which
        may be used directly to avoid having to screw with the linker options
        on your own.
        
INSTALLATION
        The library may be compiled and installed with GNU make (Or possibly
        other make utilities, but compatibility is not guaranteed) using the
        following command:
            
            $ make all

        In this version, it is intendod that this library be used as a submodule
        within a project, linking against the static library.  It is not
        significant enough in functionality to warrant polluting /usr/lib
        with extra junk.

AUTHOR
        Written by Taylor C. Richberger

REPORTING BUGS
        Please report bugs to Taywee@lavabit.com
        Project Github: <https://github.com/Taywee/GLmm>

COPYRIGHT
        Copyright © 2013 Taylor C. Richberger.  License LGPL version 3
        <http://www.gnu.org/copyleft/lesser.html>.
        
        Text of license included in LICENSE

THIS README LAST UPDATED
        2013, Nov 05


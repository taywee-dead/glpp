/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include "file.hxx"

#include <libxml++/libxml++.h>

namespace GLpp
{
    File::File()
    {
        vertices = nullptr;
        normals = nullptr;
        texCoords = nullptr;
        elements = nullptr;
    }

    File::~File()
    {
        Destroy();
    }

    void File::Destroy()
    {
        if (vertices)
        {
            delete[] (vertices);
            vertices = nullptr;
        }
        if (normals)
        {
            delete[] (normals);
            normals = nullptr;
        }
        if (texCoords)
        {
            delete[] (texCoords);
            texCoords = nullptr;
        }
        if (elements)
        {
            delete[] (elements);
            elements = nullptr;
        }
    }


    bool File::FileExists(const char* filename)
    {
        if (FILE* file = fopen(filename, "r"))
        {
            fclose(file);
            return (true);
        }
        return (false);
    }

    GLdouble* File::GetVertices() const
    {
        return (vertices);
    }

    GLdouble* File::GetNormals() const
    {
        return (normals);
    }

    GLdouble* File::GetTexCoords() const
    {
        return (texCoords);
    }

    unsigned int* File::GetElements() const
    {
        return (elements);
    }

    unsigned int File::GetVertexSize() const
    {
        return (vertexSize);
    }

    unsigned int File::GetNormalSize() const
    {
        return (normalSize);
    }

    unsigned int File::GetVertexCount() const
    {
        return (vertexCount);
    }

    unsigned int File::GetElementCount() const
    {
        return (elementCount);
    }

    GLenum File::GetVertexType() const
    {
        return (GL_DOUBLE);
    }

    GLenum File::GetNormalType() const
    {
        return (GL_DOUBLE);
    }

    GLenum File::GetElementType() const
    {
        return (GL_UNSIGNED_INT);
    }

    unsigned int File::GetVertexTypeSize() const
    {
        return (sizeof(GLdouble));
    }

    unsigned int File::GetNormalTypeSize() const
    {
        return (sizeof(GLdouble));
    }

    unsigned int File::GetElementTypeSize() const
    {
        return (sizeof(GLuint));
    }

    unsigned int File::GetVertexBufferSize() const
    {
        return (GetVertexTypeSize() * GetVertexCount() * GetVertexSize());
    }

    unsigned int File::GetNormalBufferSize() const
    {
        return (GetNormalTypeSize() * GetNormalSize() * GetVertexCount());
    }

    unsigned int File::GetElementBufferSize() const
    {
        return (GetElementTypeSize() * GetElementCount());
    }

    void File::SetMeshName(const std::string& meshName)
    {
        this->meshName = meshName;
    }

    void File::SetSkeletonName(const std::string& skeletonName)
    {
        this->skeletonName = skeletonName;
    }

    bool File::Import(const std::string& fileName)
    {
        std::string extension;
        size_t temp;
        if (!FileExists(fileName.c_str()))
        {
            return (false);
        }

        extension = ".dae";
        temp = fileName.rfind(extension);
        if ((temp != std::string::npos)
            && (temp == (fileName.length() - extension.length())))
        {
            return(ImportCollada(fileName));
        }
        return(false);
    }

    bool File::ImportCollada(const std::string& fileName)
    {
        Destroy();
        GLdouble* tempNormals = nullptr;
        xmlpp::DomParser parser;
        xmlpp::Document* doc = nullptr;
        parser.parse_file(fileName.c_str());
        xmlpp::Element* root;
        if (!parser)
        {
            return (false);
        }
        doc = parser.get_document();
        root = doc->get_root_node();
        xmlpp::Element* library_geometries = dynamic_cast<xmlpp::Element*>(root->get_children("library_geometries").front());
        xmlpp::Node::NodeList geometrylist = library_geometries->get_children("geometry");
        xmlpp::Element* geometry = nullptr;
        for (xmlpp::Node::NodeList::iterator it = geometrylist.begin(); it != geometrylist.end(); it++)
        {
            if (meshName == ((xmlpp::Element*)(*it))->get_attribute_value("name"))
            {
                geometry = dynamic_cast<xmlpp::Element*>(*it);
            }
        }
        xmlpp::Node* mesh = geometry->get_children("mesh").front();
        if (!mesh)
        {
            return (false);
        }
        xmlpp::Node::NodeList sources = mesh->get_children("source");
        xmlpp::Element* meshPositions = nullptr;
        xmlpp::Element* meshNormals = nullptr;

        for (xmlpp::Node::NodeList::iterator it = sources.begin(); it != sources.end(); it++)
        {
            Glib::ustring id = ((xmlpp::Element*)(*it))->get_attribute_value("id");
            if ((id.rfind("positions") != Glib::ustring::npos) &&
                (id.rfind("positions") == (id.size() - 9)))
            {
                meshPositions = dynamic_cast<xmlpp::Element*>(*it);
            }
            if ((id.rfind("normals") != Glib::ustring::npos) &&
                (id.rfind("normals") == (id.size() - 7)))
            {
                meshNormals = dynamic_cast<xmlpp::Element*>(*it);
            }
        }

        xmlpp::Element* positionsArray = dynamic_cast<xmlpp::Element*>(meshPositions->get_children("float_array").front());
        xmlpp::Element* positionsTechnique = dynamic_cast<xmlpp::Element*>(meshPositions->get_children("technique_common").front());
        xmlpp::Element* positionsAccessor = dynamic_cast<xmlpp::Element*>(positionsTechnique->get_children("accessor").front());

        xmlpp::Element* normalsArray = dynamic_cast<xmlpp::Element*>(meshNormals->get_children("float_array").front());
        xmlpp::Element* normalsTechnique = dynamic_cast<xmlpp::Element*>(meshNormals->get_children("technique_common").front());
        xmlpp::Element* normalsAccessor = dynamic_cast<xmlpp::Element*>(normalsTechnique->get_children("accessor").front());

        std::stringstream(positionsAccessor->get_attribute_value("stride")) >> vertexSize;
        std::stringstream(positionsAccessor->get_attribute_value("count")) >> vertexCount;

        std::stringstream positionsSS(positionsArray->get_child_text()->get_content());

        vertices = new GLdouble[vertexSize * vertexCount];
        for (unsigned int i = 0; i < (vertexSize * vertexCount); i++)
        {
            positionsSS >> vertices[i];
        }

        std::stringstream(normalsAccessor->get_attribute_value("stride")) >> normalSize;

        std::stringstream normalsSS(normalsArray->get_child_text()->get_content());
        tempNormals = new GLdouble[normalSize * vertexCount];
        for (unsigned int i = 0; i < (normalSize * vertexCount); i++)
        {
            normalsSS >> tempNormals[i];
        }

        normals = new GLdouble[vertexCount * normalSize];

        xmlpp::Element* polylist = dynamic_cast<xmlpp::Element*>(mesh->get_children("polylist").front());
        xmlpp::Node::NodeList polyInput = polylist->get_children("input");
        xmlpp::Element* polyData = dynamic_cast<xmlpp::Element*>(polylist->get_children("p").front());

        unsigned int polyCount, vertexOffset, normalOffset, vertexInputCount = 0;
        std::stringstream(polylist->get_attribute_value("count")) >> polyCount;

        elementCount = polyCount * 3;

        elements = new unsigned int[elementCount]();

        for (xmlpp::Node::NodeList::iterator it = polyInput.begin(); it != polyInput.end(); it++)
        {
            vertexInputCount++;
            xmlpp::Element* input = dynamic_cast<xmlpp::Element*>(*it);

            if (input->get_attribute_value("semantic") == "VERTEX")
            {
                std::stringstream(input->get_attribute_value("offset")) >> vertexOffset;
            } else if (input->get_attribute_value("semantic") == "NORMAL")
            {
                std::stringstream(input->get_attribute_value("offset")) >> normalOffset;
            }
        }

        std::stringstream polySS(polyData->get_child_text()->get_content());
        unsigned int* vertexInputs = new unsigned int[vertexInputCount]();

        for (unsigned int i = 0; i < elementCount; i++)
        {
            for (unsigned int j = 0; j < vertexInputCount; j++)
            {
                polySS >> vertexInputs[j];
            }
            elements[i] = vertexInputs[vertexOffset];
            for (unsigned int j = 0; j < normalSize; j++)
            {
                normals[(vertexInputs[vertexOffset] * normalSize) + j]
                    = tempNormals[(vertexInputs[normalOffset] * normalSize) + j];
            }
        }
        if (vertexInputs)
        {
            delete[] (vertexInputs);
        }
        if (tempNormals)
        {
            delete[] (tempNormals);
        }

        return (true);
    }
}

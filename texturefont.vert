#version 330
#extension GL_EXT_texture_array : enable

// Per string
uniform ivec2 windowDimensions;
uniform float size;

// Per vertex
in vec2 texCoord;
in vec2 vertex;

// Per instance
in int character;
in vec2 windowLocation;

flat out int charIndex;
out vec2 textureCoord;

void main()
{
    charIndex = character;
    textureCoord = texCoord;
    gl_Position = vec4(((windowLocation + (size * vertex)) / windowDimensions) * 2.0f - vec2(1.0f), 0.0f, 1.0f);
}

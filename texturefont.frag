#version 330
#extension GL_EXT_texture_array : enable

uniform sampler2DArray glyph;
uniform vec4 color;

flat in int charIndex;
in vec2 textureCoord;

void main()
{
    gl_FragColor = vec4(color.rgb, color.a * texture2DArray(glyph, vec3(textureCoord, charIndex)).a);
}

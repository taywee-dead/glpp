/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef TEXTUREFONT_HXX
#define TEXTUREFONT_HXX

#include <string>
#include <iostream>
#include <GL/glew.h>
#include <GL/gl.h>

namespace GLpp
{
    /**
     * \brief Class which will create and use a font in a set of textures.
     */
    class TextureFont
    {
        protected:
            /**
             * \brief Structure which holds all of the data for all of the ascii characters.
             *
             * Also holds size, bearing, and advance values.
             */
            struct Glyph
            {
                GLfloat width;
                GLfloat height;
                GLfloat xAdvance;
                GLfloat yAdvance;
                GLfloat yBearing;
                GLfloat xBearing;
            };
            Glyph texGlyphs[127];
            GLuint texture;

            static GLuint VAO;
            static GLuint vertices;
            GLuint instanceBuffer;

            struct InstanceData
            {
                GLint character;
                GLfloat windowLocationX;
                GLfloat windowLocationY;
            } *instanceData;

            GLuint instanceDataSize;

            static GLuint vertexShader;
            static GLuint fragmentShader;
            static GLuint program;

            /**
             * \brief Structure which holds uniform indexes for use with the program
             *
             * Used internally only
             */
            struct Uniforms
            {
                GLint windowDimensions;
                GLint size;
                GLint color;
            };
            static Uniforms uniforms;

            /**
             * \brief Structure which holds array indexes for use with the program
             *
             * Used internally only
             */
            struct Attribs
            {
                GLint character;
                GLint windowLocation;
            };
            static Attribs attribs;

            /**
             * Draws glyph from bitmap into image
             * 
             * \param bitmap the image as provided by freetype
             * \param image the array to write into
             * \param bitWidth the width of the bitmap
             * \param width the width of the glyph
             * \param height the height of the glyph
             */
            static GLvoid DrawGlyph(GLubyte* bitmap, GLubyte* image, const GLuint& bitWidth, const GLuint& width, const GLuint& height);


        public:
            /**
             * Does nothing.  Call Create() to create the font.
             */
            TextureFont();

            /**
             * \brief destroys all assets
             *
             * Destroys instance data
             */
            virtual ~TextureFont();

            /**
             * \brief Create static assets used across different fonts
             */
            static GLboolean Initiate();

            /**
             * \brief Destroy static assets used across different fonts
             */
            static GLvoid Cleanup();

            /**
             * \brief Create font, set all values, and creates all shaders.
             *
             * \param fontName path to the font file
             * \param the maxSize of the font file, preferably in a power of 2, otherwise
             *        it will be upscaled
             */
            GLboolean Create(const std::string& fontName, GLuint maxSize);

            /**
             * \brief draws the string to the screen
             *
             * \param text the string to draw
             * \param x horizontal location
             * \param y vertical location
             * \param size vertical size of glyph, note that actual appearing font may be smaller
             * \param reverseX boolean that determines if the x value is from the right side of the
             *        screen, will also reverse direction of line breaks and justification
             * \param reverseY boolean that determines if the y value is  determined from the top of the
             *        screen
             * \param color color to pass in as an array of floats in RGBA form
             */
            GLvoid DrawString(const std::string& text, const GLfloat& x, const GLfloat& y, const GLfloat& size, const GLboolean& reverseX, const GLboolean& reverseY, const GLfloat* color);
    };
}

#endif

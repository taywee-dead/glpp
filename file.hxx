
/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef FILE_HXX
#define FILE_HXX

#include <string>
#include <sstream>
#include <cstdio>
#include <fstream>

#include <GL/glew.h>
#include <GL/gl.h>

namespace GLpp
{
    /**
     * \brief A model file import class
     *
     * This class can, as of yet, only import Collada files, but will likely be expanded to
     * support Stanford, and maybe even Blender at some point.  Eventually a new format as well,
     * created to be specialized for single-file single-model, allowing binary file import and
     * export of a single model, as well as a skeleton, attribute data, and possibly embedded
     * png-textures.
     */
    class File
    {
        protected:
            GLdouble* vertices;
            GLdouble* normals;
            GLdouble* texCoords;
            unsigned int* elements;

            unsigned int vertexSize;
            unsigned int normalSize;

            unsigned int vertexCount;
            unsigned int elementCount;

            std::string meshName;
            std::string skeletonName;

            static bool FileExists(const char* filename);

        public:
            /**
             * Sets null pointers.
             */
            File();

            /**
             * Only calls Destroy().
             */
            virtual ~File();

            /**
             * Deletes all pointers.
             */
            void Destroy();

            /**
             * \returns Array of vertices, the size is equal to GetVertexSize() * GetVertexCount()
             */
            GLdouble* GetVertices() const;

            /**
             * \returns Array of normals, the size is equal to GetNormalSize() * GetVertexCount()
             */
            GLdouble* GetNormals() const;

            /**
             * \returns Array of texture coordinates, the size is equal to
             * GetTexCoordSize() * GetVertexCount()
             */
            GLdouble* GetTexCoords() const;

            /**
             * \returns Array of texture coordinates, the size is equal to GetElementCount()
             */
            unsigned int* GetElements() const;

            /**
             * \returns The amount of values that define a single Vertex
             */
            unsigned int GetVertexSize() const;

            /**
             * \returns The amount of values that define a single Normal
             */
            unsigned int GetNormalSize() const;

            /**
             * Unimplemented.
             * \returns Array of texture coordinates, the size is equal to
             * GetTexCoordSize() * GetVertexCount()
             */
            unsigned int GetTexCoordSize() const;

            /**
             * \returns The number of vertices in the model
             */
            unsigned int GetVertexCount() const;

            /**
             * \returns The number of elements that define the model
             * (not the number of polygons, but of individual points)
             */
            unsigned int GetElementCount() const;

            /**
             * \returns The datatype enum for vertices, will always be GL_FLOAT
             */
            GLenum GetVertexType() const;

            /**
             * \returns The datatype enum for normals, will always be GL_FLOAT
             */
            GLenum GetNormalType() const;

            /**
             * \returns The datatype enum for elements, will always be GL_UNSIGNED_INT
             */
            GLenum GetElementType() const;

            /**
             * \returns The size of vertex points in bytes, will always be sizeof(GLdouble)
             */
            unsigned int GetVertexTypeSize() const;

            /**
             * \returns The size of normal points in bytes, will always be sizeof(GLdouble)
             */
            unsigned int GetNormalTypeSize() const;

            /**
             * \returns The size of normal points in bytes, will always be sizeof(GLuint)
             */
            unsigned int GetElementTypeSize() const;

            /**
             * \brief returns the size to be used for Vertex Buffers.
             *
             * Only provided as a convenience function, easily calculated by the programmer, but
             * also easy to slip up.
             *
             * \returns GetVertexTypeSize() * GetVertexCount() * GetVertexSize()
             */
            unsigned int GetVertexBufferSize() const;

            /**
             * \brief returns the size to be used for Normal Buffers.
             *
             * Only provided as a convenience function, easily calculated by the programmer, but
             * also easy to slip up.
             *
             * \returns GetNormalTypeSize() * GetVertexCount() * GetNormalSize()
             */
            unsigned int GetNormalBufferSize() const;

            /**
             * \brief returns the size to be used for Element Buffers.
             *
             * Only provided as a convenience function, easily calculated by the programmer, but
             * also easy to slip up.
             *
             * \returns GetElementTypeSize() * GetElementCount()
             */
            unsigned int GetElementBufferSize() const;

            /**
             * \brief Sets the Mesh name.
             *
             * This is used in the file import methods when the mesh's name is relevant,
             * such as Collada, or most formats which allow for multiple models.
             *
             * \param meshName the mesh's name in string format, will likely be changed to a UTF8 format
             */
            void SetMeshName(const std::string& meshName);

            /**
             * \brief Sets the Skeleton name.
             *
             * This is used in the file import methods when the skleton's name is relevant,
             * such as Collada, or most formats which allow for multiple models.
             *
             * \param skeletonName the skeleton's name in string format, will likely be changed to a UTF8 format
             */
            void SetSkeletonName(const std::string& skeletonName);

            /**
             * \brief Imports a file document.
             *
             * This will attempt to discern the filetype from the extension, and use the proper
             * Import function to parse it.
             *
             * \returns boolean indicating success
             */
            bool Import(const std::string& fileName);

            /**
             * \brief Imports a collada (.dae) document
             *
             * Uses libxml++ to import a Collada document, requires the mesh name and skeleton
             * name to be set.  If the skeleton name is blank, the skeleton will not be imported.
             *
             * \returns boolean indicating success
             */
            bool ImportCollada(const std::string& fileName);
    };
}
#endif
